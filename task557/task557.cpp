#include "stdafx.h"
#include <malloc.h>

int* UmnMatrix(int n, int *m1, int *m2) {
	int *out = (int*)malloc(n*n * sizeof(int));
	
	int sum;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			sum = 0;
			for (int k = 0; k < n; ++k)
				sum += *(m1 + i * n + k) * (*(m2 + k * n + j));
			*(out + i * n + j) = sum;
		}
	}
	return out;
}

int localUmn(int n, int *m1, int *m2) {
	return 404;
}

void printMass(int n, int* mass) {
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			printf("%d ", *(mass + i * n + j));
		}
		printf("\n");
	}
}

int* InputMass(int n) {

	int *a = (int*)malloc(n*n * sizeof(int));
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			printf("a[%d][%d] = ", i, j);
			scanf_s("%d", (a + i * n + j));
		}
	}
	return a;
}


int main()
{
	int n;
	scanf_s("%d", &n);

	int *a = InputMass(n);
	int *b = InputMass(n);

	int *c = UmnMatrix(n, a, b); //test
	printMass(n, c);

	return 0;
}

